Duckwing vs Pirate Santa
------------------------

    Berlin Mini Game Jam November 2016
    Art:            Michael Hussinger
    Code & SFX:     Christiaan Janssen
    Music:          TMG - TelStar 1.9.1 (Creative Commons - Attribution CC-BY)
    Game Enigne:    Löve2D v0.10.2 ( http://love2d.org )
    License:        Creative Commons - Attribution - Share Alike (CC-BY-SA)


Game Controls
-------------

    cursor keys / wasd                Move
    ctrl / shift / space              Shoot
    double tap on a  direction        Dash


Special keys
------------

    ESC / Alt+f4      Exit
    F11               Switch fullscreen
    F3                Toggle sound on/off
    +                 Increase resolution (Windowed mode only)
    -                 Decrease resolution (Windowed mode only)

Commandline parameters
----------------------

    -fullscreen               Start in fullscreen mode
    -vsync                    Switch vsync (Fullscreen mode only)
    -nosound                  Start in silent mode
    -original-resolution      Start in 300x200 resolution


