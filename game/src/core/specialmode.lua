
SpecialMode = SpecialMode or {}

function SpecialMode.init()
    SpecialMode.Timers = Timers.newInstance()
end

function SpecialMode.reset()
    SpecialMode.setState("intro")
    SpecialMode.center = Vector(150,100)
    SpecialMode.radius = 0
end

function SpecialMode.setState(newStateName)
    SpecialMode.gameState = newStateName

    if newStateName == "intro" then
        local introTime = 3.5
        SpecialMode.Timers.create(introTime)
            :prepare(function()
                startPlayer()
                Boss.reset()
                Player.killAnims()
                Player.hide()
                Player.pos.x = 260
                Boss.pos = Vector(250, 80)
                Bullets.wipe()
            end)
            :withUpdate(function(elapsed)
                SpecialMode.radius = elapsed/introTime * 185
            end)
            :andThen(function()
                SpecialMode.setState("playing")
                -- Boss.reset()
                -- Boss_WanderState:reset()
            end)
            :thenWait(0.2):andThen(function()
                Player.spawn()
            end)
            :start()
    elseif newStateName == "gameover" or newStateName == "win" then
        local outroTime = 2
        local txt = newStateName == "win" and "YOU WIN" or "YOU LOSE"
        SpecialMode.Timers.create(outroTime)
            :prepare(function()
                Player.alive = false
            end)
            :withUpdate(function(elapsed)
                SpecialMode.radius = (1-elapsed/outroTime) * 185
            end)
            :thenWait(4)
            :prepare(newStateName == "win" and sfxWin or sfxLose)
            :withDraw(function()
                love.graphics.setFont(font)
                love.graphics.setColor(224, 98, 248)
                local w = font:getWidth(txt)
                printText(txt, math.floor(150 - w*0.5), 60)
            end)
            :andThen(function()
                SpecialMode.setState("intro")
            end)
            :start()
    end

    if newStateName == "intro" then
        startMusic()
    elseif newStateName == "gameover" or newStateName == "win" then
        fadeoutMusic()
    end
end

function SpecialMode.update(dt)
    SpecialMode.Timers.update(dt)
end

function SpecialMode.draw()
    if SpecialMode.gameState ~= "playing" then
        if SpecialMode.radius >= 1 then
            love.graphics.stencil(function()
                love.graphics.circle("fill",SpecialMode.center.x, SpecialMode.center.y, SpecialMode.radius)
                end, "replace", 1)
            love.graphics.setStencilTest("equal", 0)
        end

        love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill",0,0,300,200)

        love.graphics.setStencilTest()

        SpecialMode.Timers.draw()
    end

end
