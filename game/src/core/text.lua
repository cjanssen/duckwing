
function loadFont()
    font = love.graphics.newImageFont("img/gamefont_trn.png","ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890.",1)
    love.graphics.setFont(font)
end

function printText(txt, x, y)
    txt = txt:upper()
    local r,g,b,a = love.graphics.getColor()
    local textBgColor = {18,33,18,255}
    love.graphics.setColor(textBgColor)
    local w,h = font:getWidth(txt),font:getHeight()
    love.graphics.rectangle("fill",x-1,y,w+1,h)
    love.graphics.rectangle("fill",w+x,y+1,1,h-1)
    love.graphics.rectangle("fill",x,y+h,w,1)
    love.graphics.setColor(r,g,b,a)
    Shaders.enableTextShader()
    love.graphics.print(txt, x, y)
    Shaders.clear()
    return w,h
end
