Sound = Sound or {}

function initSound()
	Sound.active = true
	loadSFX()
	loadMusic()
end

function loadSFX()
	Sound.sfx = {}
end

function loadMusic()
	Sound.music = love.audio.newSource("music/TMG-TelStar.ogg","stream")
	Sound.music_vol = 1
	Sound.music:setLooping(true)
end

function startMusic()
	Sound.music:stop()
	Sound.music:setVolume(Sound.music_vol)
	Sound.music:play()
end

function fadeoutMusic()
	local fadeout_time = 2
	Timers.create(fadeout_time):withUpdate(function(elapsed)
		Sound.music:setVolume(Sound.music_vol * math.max(0, 1 - elapsed/fadeout_time))
	end)
	:andThen(function() Sound.music:stop() end)
	:start()
end

function sfxDo(snd_ndx, vol, special)
	if not Sound.active then return end

	if not special and SpecialMode.gameState ~= "playing" then return end

	if not Sound.sfx[snd_ndx] then
		Sound.sfx[snd_ndx] = love.audio.newSource("sfx/"..snd_ndx..".ogg")
	end
	vol = vol or 1
	Sound.sfx[snd_ndx]:setVolume(vol or 1)
	Sound.sfx[snd_ndx]:clone():play()
end

function toggleSound()
	Sound.active = not Sound.active
	if Sound.music:isPlaying() then
		Sound.music:pause()
	else
		Sound.music:play()
	end
end

--------------------------------------------------

function sfxPlayerShoot()
	sfxDo("fx_player_shoot", 0.4)
end

function sfxBossHit()
	sfxDo("fx_boss_hit", 0.28)
end

function sfxBossBigBullet()
	sfxDo("fx_boss_big_bullet", 0.4)
end

function sfxPlayerHit()
	sfxDo("fx_player_hit", 0.2)
end

function sfxPlayerSpawn()
	sfxDo("fx_player_spawn", 0.4)
end

function sfxPlayerDash()
	sfxDo("fx_player_dash", 0.4)
end

function sfxBossStageHit()
	sfxDo("fx_boss_mini_explo", 0.4)
end

function sfxSantaFall()
	sfxDo("fx_santa_fall", 0.4)
end


function sfxPlayerExit()
	sfxDo("fx_player_leave", 0.4)
end

function sfxBossRam()
	sfxDo("fx_boss_ram", 0.8)
end

function sfxBossChargeLife()
	sfxDo("fx_boss_charge_life", 0.4)
end

function sfxBossBarrage()
	sfxDo("fx_boss_barrage", 0.28)
end

function sfxBossInverseRam()
	sfxDo("fx_boss_inverse_ram", 0.8)
end

function sfxBossSnipeCharge()
	sfxDo("fx_boss_snipe_charge", 0.2)
end

function sfxBossSnipeShoot()
	sfxDo("fx_boss_snipe_shoot", 0.6)
end

function sfxBossShootHoming()
	sfxDo("fx_boss_shoot_homing", 0.8)
end

function sfxHomingExplode()
	sfxDo("fx_homing_explode", 0.4)
end

function sfxDeployHelli()
	sfxDo("fx_heli_open", 0.4)
end

function sfxCloseHelli()
	sfxDo("fx_heli_close", 0.4)
end

function sfxWin()
	sfxDo("fx_end_win", 0.4,true)
end

function sfxLose()
	sfxDo("fx_end_lose", 0.6,true)
end
