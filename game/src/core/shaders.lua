Shaders = Shaders or {}

function Shaders.enableWhiteSpriteShader()
    if not Shaders.whiteSpriteShader then
        Shaders.whiteSpriteShader = love.graphics.newShader [[
            vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
            {
                color.w = texture2D(texture, texture_coords).w;
                return color;
            }
        ]]
    end

    love.graphics.setShader(Shaders.whiteSpriteShader)
end

function Shaders.enableTextShader()
    if not Shaders.fontShader then
        Shaders.fontShader = love.graphics.newShader [[
            vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
            {
                vec4 outv = texture2D(texture, texture_coords);
                if (outv.w > 0.0 && outv.x<0.9) {
                    outv = color;
                }
                return outv;
            }
        ]]
    end

    love.graphics.setShader(Shaders.fontShader)
end

function Shaders.enableLaserShader(center, radius)
    if not Shaders.laserShader then
        Shaders.laserShader = love.graphics.newShader [[
            uniform float center;
            uniform float radius;
            vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
            {
                float alpha = 1 - abs(screen_coords.x - center)/radius;
                color.w = alpha * alpha;
                return color;
            }
        ]]
    end
    love.graphics.setShader(Shaders.laserShader)
    Shaders.laserShader:send("center", center)
    Shaders.laserShader:send("radius", radius)
end

function Shaders.enableFlipChannelShader(channelMode)
    if not Shaders.flipChannelShader then
        Shaders.flipChannelShader = {}
    end
    channelMode = channelMode:lower()
    if not Shaders.flipChannelShader[channelMode] then
        -- todo: rbg, grb
        if channelMode == "gbr" then
            Shaders.flipChannelShader[channelMode] = love.graphics.newShader [[
                vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
                {
                    vec4 outv = texture2D(texture, texture_coords);
                    return color * vec4(outv.y,outv.z,outv.x,outv.w);
                }
            ]]
        elseif channelMode == "brg" then
            Shaders.flipChannelShader[channelMode] = love.graphics.newShader [[
                vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
                {
                    vec4 outv = texture2D(texture, texture_coords);
                    return color * vec4(outv.z,outv.x,outv.y,outv.w);
                }
            ]]
        elseif channelMode == "bgr" then
            Shaders.flipChannelShader[channelMode] = love.graphics.newShader [[
                vec4 effect(vec4 color, sampler2D texture, vec2 texture_coords, vec2 screen_coords)
                {
                    vec4 outv = texture2D(texture, texture_coords);
                    return color * vec4(outv.z,outv.y,outv.x,outv.w);
                }
            ]]
        end
    end

    love.graphics.setShader(Shaders.flipChannelShader[channelMode])
end


function Shaders.clear()
    love.graphics.setShader()
end