Scroll = Scroll or {}

function initScroll()
    Scroll.size = Vector(500, 200)
    Scroll.backgroundFrac = 0.5
    Scroll.layers = {
        back = Vector(),
        mid = Vector(),
        top = Vector()
    }
end

function updateScroll(dt)
    if not Player.spawning then
        local pos = Player.pos:clamp(Player.moveLimit:topLeft(), Player.moveLimit:bottomRight())
        adjustScroll(pos)
    end
end

function adjustScroll(pos)
    -- reverse-solved the scroll equations... don't ask
    local ofs = Scroll.size.x - screenSize.x
    local z = Scroll.backgroundFrac*1.25*1.25
    local magic = z * ofs / screenSize.x
    local tx = (magic * pos.x + 0.5) / (1+magic)
    local vx = pos.x - tx


    local f0 = (vx/screenSize.x - 0.5)

    local frac = f0 * Scroll.backgroundFrac + 0.5
    Scroll.layers.back.x = ofs * frac

    local frac = f0  *  Scroll.backgroundFrac*1.25 + 0.5
    Scroll.layers.mid.x = ofs * frac

    frac = f0 *  z + 0.5
    Scroll.layers.top.x = ofs * frac

end