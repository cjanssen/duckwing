Boss = Boss or {}

local function loadBehaviors()
    love.filesystem.load("src/boss/states/wanderstate.lua")()
    love.filesystem.load("src/boss/states/ramstate.lua")()

    love.filesystem.load("src/boss/states/spreadstate.lua")()
    love.filesystem.load("src/boss/states/inverseramstate.lua")()

    love.filesystem.load("src/boss/states/homingballstate.lua")()
    love.filesystem.load("src/boss/states/sniperstate.lua")()

    love.filesystem.load("src/boss/states/helicopterstate.lua")()
    love.filesystem.load("src/boss/states/squareramstate.lua")()
    love.filesystem.load("src/boss/states/zigzagspreadstate.lua")()

    love.filesystem.load("src/boss/states/hurtstate.lua")()
    love.filesystem.load("src/boss/states/deathstate.lua")()
end

function Boss.init()
    loadBehaviors()


    Boss.pos = Vector(250, 80)
    Boss.size = Vector(64,64)

    Boss.setState(Boss_WanderState)
    Boss.initGraphics()
    Boss.hitFrameCount = 0
    Boss.maxLive = 64*24
    Boss.liveStageChange =  math.floor(Boss.maxLive/4)
    Boss.flipped = false
    Boss.vulnerable = true
	Boss.visible = true
    Boss.bottomOfs = Vector()

    Boss.stateProgression = {
        Boss_WanderState,
        Boss_SpreadState,
        Boss_HomingBallState,
        Boss_ZigZagSpreadState,
        Boss_DeathState
    }
end

function Boss.reset()
    Boss.vulnerable = true
    Boss.stateIndex = 1
    Boss.setState(Boss.stateProgression[Boss.stateIndex])
    Boss.hitFrameCount = 0
    Boss.lives = Boss.maxLive
    Boss.lives_display = Boss.liveStageChange
    Boss.visible = true
    Boss.flipped = false
end

function Boss.initGraphics()
    local img = love.graphics.newImage("img/bosssheet.png")
    local w,h = img:getWidth(),img:getHeight()
    Boss.batch = love.graphics.newSpriteBatch(img, 8)
    local function update(self, dt)
        self.timer = (self.timer + dt) % (#self.frames * self.frameTime)
        self.frame = math.floor(self.timer / self.frameTime) + 1
    end
    Boss.spriteSize = Vector(128, 128)
    Boss.anim = {}
    Boss.anim.top = {
        frames = {},
        frame = 1,
        frameTime = 0.25,
        timer = 0,
        update = update
    }
    Boss.anim.bottom = {
        frames = {},
        frame = 1,
        frameTime = 0.045,
        timer = 0,
        update = update
    }

    for x=0,128,128 do
        table.insert(Boss.anim.top.frames, love.graphics.newQuad(x,0,128,128,w,h))
        table.insert(Boss.anim.bottom.frames, love.graphics.newQuad(x,128,128,128,w,h))
    end

    Boss.anim.hitFrame = love.graphics.newQuad(256,128,128,128,w,h)
end

function Boss.createResetAnim(bossDest, minDelay, moveSpeed)
    minDelay = minDelay or 0.4
    moveSpeed = moveSpeed or 200
    local currentPos = Boss.pos:copy()
    local dist = bossDest - currentPos
    local resetTime = math.max(minDelay, dist:mod() / moveSpeed)
    return Timers.create(resetTime):withUpdate(function(elapsed)
        Boss.pos = currentPos + dist * elapsed / resetTime
        end):andThen(function() Boss.pos = bossDest end)
end

function Boss.setState(state)
    if Boss.state then
        if Boss.state.anim then Boss.state.anim:cancel() end
        if Boss.state.leave then Boss.state:leave() end
    end
    Boss.state = state
    state:launch()
    if state.anim then state.anim:start() end
end

function Boss.update(dt)
    Boss.hitThisTurn = false

    if Boss.state.update then
        Boss.state:update(dt)
    end

    --- shots
    Boss.checkBullets()

    Boss.anim.top:update(dt)
    Boss.anim.bottom:update(dt)

end

function Boss.draw()
    love.graphics.push()
    love.graphics.translate((-Scroll.layers.top):floor():unpack())

    if Boss.visible then
        if Boss.hitFrameCount > 0 then
            Shaders.enableWhiteSpriteShader()
        end
        local yscale = 1
        if Boss.flipped then yscale = -1 end
        local pos = (Boss.pos + Boss.bottomOfs):floor()
        love.graphics.draw(Boss.batch:getTexture(), Boss.anim.bottom.frames[Boss.anim.bottom.frame],
            pos.x, pos.y, 0, 1, yscale, Boss.spriteSize.x*0.5, Boss.spriteSize.y*0.5)
        if Boss.hitFrameCount > 0 then
            Shaders.clear()
        end
        local yofs = 0
        if Boss.flipped then yofs = 40  end
        love.graphics.draw(Boss.batch:getTexture(), Boss.anim.top.frames[Boss.anim.top.frame],
            Boss.pos.x, Boss.pos.y + yofs, 0, 1, 1, Boss.spriteSize.x*0.5, Boss.spriteSize.y*0.5)
    end
    love.graphics.pop()


    -- state special draw
    if Boss.state.draw then
        Boss.state:draw()
    end
end

local BossLiveHues = { 35, 180, 70, 319, 5, 99, 285, 145 }

function Boss.drawDisplay()
    if Boss.lives > 0 then
        local y0 = 145
        local group = math.ceil(Boss.lives/Boss.liveStageChange)
        local y1 = math.ceil(Boss.lives_display/Boss.liveStageChange * 128)
        love.graphics.setColor(hsv2rgb(BossLiveHues[group], 0.43 * 255, 0.84*255))
        love.graphics.rectangle("fill",300-9, y0-y1, 5, y1)
        love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill",300-9, y0-128, 5, 128-y1)
    else
        love.graphics.setColor(0,0,0)
        love.graphics.rectangle("fill",300-9, 145-128, 5, 128)
    end
end

function Boss.checkBullets()
    if Boss.vulnerable and Boss.visible then
        for i,v in ipairs(Bullets.playerList) do
            if checkBoxes(v, Boss) then
                Boss.hit()
                Bullets.scheduleKill(Bullets.playerList,i,false)
            end
        end
    end
end

function Boss.hit()
    if Boss.lives > 0 then
        local hitTime = 0.03
        Timers.create(hitTime):prepare(function()
            Boss.hitFrameCount = Boss.hitFrameCount + 1
            end)
            :andThen(function()
                Boss.hitFrameCount = Boss.hitFrameCount - 1
            end):start()

        -- prevent playing the same sound of top of itself in the same iteration of the main loop
        if not Boss.hitThisTurn then
            Boss.hitThisTurn = true
            sfxBossHit()
        end

        Boss.lives = math.max(0, Boss.lives-1)
        Boss.lives_display = Boss.lives % Boss.liveStageChange
        if Boss.lives%Boss.liveStageChange == 0 then
            Boss.lives_display = 0
            Boss.stateIndex = Boss.stateIndex + 1
            Boss_HurtState:setup(Boss.stateProgression[Boss.stateIndex], Boss.stateIndex-1)
            Boss.setState(Boss_HurtState)
        end
    end
end
