Boss_ZigZagSpreadState = Boss_ZigZagSpreadState or {}

function Boss_ZigZagSpreadState.launch(self)
    self.anim = Boss.createResetAnim(Vector(100 + math.random(200) - 1, 32), 1, 100)

    local desiredBalls = {}
    local ballsPerRow = 13.75
    local shootDelay = 3
    local totalRows = 7
    local totalBalls = ballsPerRow * totalRows
    local ballspeed = 150
    for i=0,totalBalls do
        local ix = (i%(2*ballsPerRow))
        local arc = 0.5*(1 + math.cos(math.pi * ix / ballsPerRow))
        local ang = math.pi * (0.05 + 0.9*arc)

        table.insert(desiredBalls, {
            angle = ang,
            time = i/totalBalls*shootDelay
        })
    end


    local soundDecimation = 3
    local soundCount = soundDecimation - 1
    local shootAnim = Timers.create(shootDelay)
        :withUpdate(function(elapsed)
            local sounded = false
            while #desiredBalls > 0 and desiredBalls[1].time <= elapsed do
                local newBall = table.remove(desiredBalls,1)
                local radius = ballspeed * (elapsed - newBall.time)
                Bullets.addEnemyDirBulletSmall(Boss.pos + VectorFromPolar(radius, newBall.angle), newBall.angle)
                if not sounded then
                    sounded = true
                    soundCount = (soundCount + 1) % soundDecimation
                    if soundCount == 0 then
                        sfxBossBarrage()
                    end
                end
            end
        end)

    self.anim:hang(shootAnim):thenWait(1):andThen(function()
        if math.random()<0.5 then
            Boss.setState(Boss_SquareRamState)
        else
            Boss.setState(Boss_HelicopterState)
        end
    end)


end

