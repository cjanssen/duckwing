
------------------------------------------------------------------
Boss_HelicopterState = Boss_HelicopterState or {}

function Boss_HelicopterState.launch(self)
    local pendulumWidth = 120
    local pendulumLow = 64 + 24
    local pendulumHi = 32

    -- move to desired position
    self.anim = Boss.createResetAnim(Vector(250, pendulumLow), 0.6, 100)

    -- boss movement
    local pendulumLoops = 2
    local pendulumTime = 4
    pendulum = Timers.create(pendulumTime)
        :withUpdate(function(elapsed)
            local frac = math.sin(math.pi*2*elapsed/pendulumTime)
            local ang = (frac+1)*math.pi*0.5
            Boss.pos.x = 250 + pendulumWidth*math.cos(ang)
            Boss.pos.y = pendulumHi + (pendulumLow - pendulumHi)*math.sin(ang)
        end):loopNTimes(pendulumLoops)

    self.helix = {
        radius = 0,
        span = 96,
        revolutions = 1,
        startAngle = math.random()*math.pi*2,
        angle = 0,
        separation = 12,
        bullets = {}
    }

    local helix = self.helix

    local deployTime = 1.5
    local awayTime = 1.5
    local rotateTime = pendulumTime * pendulumLoops

    -- helixes

    -- deploy
    local helixDeploy = Timers.create(deployTime)
        :prepare(function()
            Bullets.enemyList = {}
            Bullets.solidCount = 0

            self.helix.bullets = {}
            local minorCount = math.ceil(self.helix.span/self.helix.separation)*4
            for i = 1,minorCount do
                self.helix.bullets[8+i] = Bullets.addEnemyStaticBullet(Boss.pos,0.5)
            end
            for i = 1,8 do
                self.helix.bullets[i] = Bullets.addEnemyStaticBullet(Boss.pos, 1)
            end
            sfxDeployHelli()
        end)
        :withUpdate(function(elapsed)
            helix.radius = elapsed/deployTime*helix.span
            end)

    -- animate
    local helixAnim = Timers.create(rotateTime)
        :withUpdate(function(elapsed)
            helix.angle = (elapsed/rotateTime*helix.revolutions*math.pi*2) % (math.pi*2)
        end)
        :andThen(function()
            helix.angle = 0
            sfxCloseHelli()
        end)
        :thenWait(awayTime):withUpdate(function(elapsed)
            helix.radius = helix.span * (1-elapsed/awayTime)
        end)
        :andThen(function()
            -- clean all bullets
            Bullets.enemyList = {}
        end)
        :andThen(function()
            -- Boss.setState(Boss_SquareRamState)
            if math.random()<0.5 then
                Boss.setState(Boss_SquareRamState)
            else
                Boss.setState(Boss_ZigZagSpreadState)
            end
        end)

    -- build tree
    self.anim:append(helixDeploy)
    helixDeploy:append(pendulum)
    helixDeploy:append(helixAnim)
end

function Boss_HelicopterState.update(self, dt)
    local helix = self.helix

    if #self.helix.bullets > 0 then
        -- clockwise
        local j = 9
        local function updateNext(ang, bias)
            local aspadir = VectorFromAngle(ang)
            for i=12+bias,self.helix.radius-8,24 do
                helix.bullets[j].invisible = false
                self.helix.bullets[j].pos = Boss.pos + aspadir * i
                j = j+1
            end
        end

        for aspa=0,3 do
            local ang = helix.startAngle + helix.angle + aspa * math.pi * 0.5
            updateNext(ang, 0)
            self.helix.bullets[aspa+1].pos = Boss.pos + VectorFromPolar(self.helix.radius, ang)
        end
        for aspa=0,3 do
            local ang = helix.startAngle - helix.angle + aspa * math.pi * 0.5
            updateNext(ang, 12)
            self.helix.bullets[aspa+5].pos = Boss.pos + VectorFromPolar(self.helix.radius, ang)
        end

        -- bring back remaining bullets
        for i=j,#helix.bullets do
            helix.bullets[i].invisible = true
        end
    end
end

function Boss_HelicopterState.leave(self)
    local anyExploding = false
    for i,v in ipairs(Bullets.enemyList) do
        if v.solid then
            anyExploding = true
            v.explodes = true
            Bullets.scheduleKill(Bullets.enemyList, i, false)
        end
    end

    if anyExploding then
        sfxHomingExplode()
    end
end