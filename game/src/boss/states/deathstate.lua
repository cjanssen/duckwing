Boss_DeathState = Boss_DeathState or {}
function Boss_DeathState.launch(self)
    Boss.vulnerable = false
    Boss.visible = false

    local firstFallDelay = 1.5
    local hangDelay = 0.75
    local secondFallDelay = 2
    local exploDelay = 0.025

    local shipDist = Vector(-50, 232)
    self.shipPos = Boss.pos:copy()
    self.guyPos = Boss.pos - Vector()
    self.guyAngle = 0
    rotVel = 1
    local shipFall = Timers.create(firstFallDelay)
        :withUpdate(function(elapsed)
            local frac = elapsed/firstFallDelay
            self.shipPos.x = Boss.pos.x + frac*shipDist.x
            self.shipPos.y = Boss.pos.y + math.pow(frac,3)*shipDist.y
        end)

    local soundDecimation = 1
    local exploAnim = Timers.create(exploDelay):andThen(function()
        Explo.addSmall(self.shipPos + Vector(math.random()-0.5,math.random()-0.5) ^ Boss.size)
        soundDecimation = (soundDecimation + 1)%4
        if soundDecimation == 0 and self.shipPos.y < 224 then
            sfxBossStageHit()
        end
    end):loopObserve(shipFall):start()

    local guyFall = Timers.create(hangDelay)
        :thenWait(secondFallDelay)
        :prepare(sfxSantaFall)
        :withUpdate(function(elapsed)
            local frac = elapsed/firstFallDelay
            self.guyPos.y = Boss.pos.y + math.pow(frac,3)*shipDist.y
            self.guyAngle = frac * rotVel * math.pi*2
        end)

    local leaveWait = 0.8
    local leaveDelay = 1.7
    local spawnFrameDelay = 0.03
    local playerOrigin = Player.pos:copy()
    local duckLeave = Timers.create(leaveWait)
        :thenWait(leaveDelay)
        :prepare(function()
            Player.spawning = true
            playerOrigin = Player.pos:copy()
            sfxPlayerExit()
        end)
        :withUpdate(function(elapsed)
            local frac = elapsed/leaveDelay
            Player.pos = playerOrigin - Vector(0, math.pow(frac,5)*300)
            Player.spawnFrame = math.floor(elapsed / spawnFrameDelay) % #Player.spawnFrames + 1
        end)

    self.anim = Timers.create()
    self.anim:append(shipFall)
    guyFall:append(duckLeave)
    self.anim:append(guyFall)
    self.anim:finally(function()
        SpecialMode.setState("win")
    end)
end

-- function Boss_DeathState.update(self, dt)
-- end

function Boss_DeathState.draw(self)
    love.graphics.push()
    love.graphics.translate((-Scroll.layers.top):floor():unpack())

    love.graphics.draw(Boss.batch:getTexture(), Boss.anim.bottom.frames[Boss.anim.bottom.frame],
        self.shipPos.x, self.shipPos.y, 0, 1, 1, Boss.spriteSize.x*0.5, Boss.spriteSize.y*0.5)

    local yofs = 30
    love.graphics.draw(Boss.batch:getTexture(), Boss.anim.top.frames[Boss.anim.top.frame],
        self.guyPos.x, self.guyPos.y - yofs, self.guyAngle, 1, 1, Boss.spriteSize.x*0.5, Boss.spriteSize.y*0.5 - yofs)
    love.graphics.pop()
end

-- function Boss_DeathState.leave(self)
-- end