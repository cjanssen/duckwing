
---------------------------------------------------
Boss_SniperState = Boss_SniperState or {}
function Boss_SniperState.launch(self)
    self.anim = Boss.createResetAnim(Vector(100 + math.random(200) - 1, 64))

    local snipePositions = map(getPermutation(10), function(a) return 60 + 40*(a-1) end)

    self.snipeRays = {}

    local function checkAnimationDone()
        for i=#self.snipeRays,1,-1 do
            if self.snipeRays[i].done then
                table.remove(self.snipeRays, i)
            end
        end

        if #self.snipeRays == 0 then
            Boss.setState(Boss_HomingBallState)
        end
    end

    local function getSnipeAnim()
        -- local snipePrewait = math.random()*0.5+0.1
        local snipePrewait = math.random()*0.6 + 0.2
        local snipePrepare = 1.25
        local snipeHue = math.random(360)
        local snipeBlinkDelay = 0.035
        local rel_snipeBlinkDelay = snipeBlinkDelay / snipePrepare
        local snipeShootDelay = 0.8
        local ray = {
            visible = false,
            snipePos = table.remove(snipePositions, 1),
            snipeBlink = false,
            snipeRayColor = {hsv2rgb(snipeHue, 0.23 * 255, 0.94*255)},
            snipeShootColor = {hsv2rgb(snipeHue, 0.53 * 255, 0.74*255)},
            snipeRadius = 50,
            snipeOpacity = 0,
            shootRadius = 0
        }
        table.insert(self.snipeRays, ray)
        local snipeIndex = #self.snipeRays

        return Timers.create(snipePrewait):thenWait(snipePrepare)
            :prepare(function()
                ray.visible = true
                ray.charging = true
                sfxBossSnipeCharge()
            end)
            :withUpdate(function(elapsed)
                local frac = elapsed/snipePrepare
                ray.snipeBlink = math.floor(math.pow(frac,1.2)/snipeBlinkDelay)%2 ~= 0
                ray.snipeOpacity = frac
                end)
            :andThen(function()
                sfxBossSnipeShoot()
                ray.snipeBlink = false
                ray.blankFrame = true
                ray.shootRadius = ray.snipeRadius
                ray.shooting = true
                ray.charging = false
            end)
            :thenWait(snipeBlinkDelay)
            :andThen(function()
                ray.blankFrame = false
                ray.shootFrame = true
            end)
            :thenWait(snipeShootDelay)
            :withUpdate(function(elapsed)
                local frac = (1 - elapsed/snipeShootDelay)
                ray.shootRadius = frac * frac * ray.snipeRadius
                end)
            :andThen(function()
                ray.shooting = false
                ray.done = true
                checkAnimationDone()
            end)
    end

    -- 5 rays
    self.anim
        :hang(getSnipeAnim():ref())
        :hang(getSnipeAnim():ref())
        :hang(getSnipeAnim():ref())
        :hang(getSnipeAnim():ref())
        :hang(getSnipeAnim():ref())
        :hang(getSnipeAnim():ref())

end

function Boss_SniperState.draw(self)
    love.graphics.push()
    love.graphics.translate((-Scroll.layers.top):floor():unpack())
    for _,ray in ipairs(self.snipeRays) do
        if ray.visible then
            if ray.charging then
                local r,g,b = unpack(ray.snipeRayColor)
                if ray.snipeBlink then
                    love.graphics.setColor(r,g,b,255)
                    love.graphics.rectangle("fill",ray.snipePos,0,1,screenSize.y)
                end
                love.graphics.setColor(r,g,b,16*ray.snipeOpacity)
                love.graphics.rectangle("fill",ray.snipePos - ray.snipeRadius*0.5,0,ray.snipeRadius,screenSize.y)
                love.graphics.rectangle("fill",ray.snipePos - ray.snipeRadius,0,ray.snipeRadius*2,screenSize.y)
            elseif ray.shooting and ray.shootRadius > 0 then
                local rayColor
                if ray.blankFrame then
                    love.graphics.setColor(255,255,255)
                    love.graphics.rectangle("fill",ray.snipePos - ray.shootRadius, 0, ray.shootRadius*2, screenSize.y)
                    love.graphics.setColor(255,255,255,128)
                    love.graphics.rectangle("fill",ray.snipePos - ray.shootRadius*1.1, 0, ray.shootRadius*2.2, screenSize.y)
                elseif ray.shootFrame then
                    local r,g,b = unpack(ray.snipeRayColor)
                    local inRad = math.max(ray.shootRadius - 5,0.5)
                    love.graphics.setColor(r,g,b,128)
                    love.graphics.rectangle("fill",ray.snipePos - ray.shootRadius, 0, ray.shootRadius*2, screenSize.y)
                    love.graphics.setColor(r,g,b,256)
                    love.graphics.rectangle("fill",ray.snipePos - inRad, 0, inRad*2, screenSize.y)
                end
                -- Shaders.clear()
            end
        end
    end
    love.graphics.pop()
end

function Boss_SniperState.update(self, dt)
    for _,ray in ipairs(self.snipeRays) do
        if ray.visible and ray.shootRadius > 2 then
            if Player.vulnerable() and
                Player.pos.x + Player.size.x*0.5 > ray.snipePos - ray.shootRadius and
                Player.pos.x - Player.size.x*0.5 < ray.snipePos + ray.shootRadius then
                Player.kill()
            end
        end
    end
end
