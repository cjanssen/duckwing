
---------------------------------------------------------
Boss_SpreadState = Boss_SpreadState or {}

function Boss_SpreadState.launch(self)
    self.anim = Boss.createResetAnim(Vector(100 + math.random(200) - 1, 64), 1, 100)

    local waveChoice = math.random(3)

    local shootFunc = function(separator, ofs)
        sfxBossBarrage()
        for ang = separator - ofs,0,-1 do
            Bullets.addEnemyDirBulletSmall(Boss.pos - Vector(0,16), ang*math.pi/separator)
        end
    end

    local getShootWave
    if waveChoice == 1 then
        getShootWave = function()
            local shootAnimRate = 0.3
            return Timers.create(shootAnimRate):andThen(function()
                shootFunc(8, 0)
            end):thenWait(shootAnimRate):andThen(function()
                shootFunc(8, 0.5)
            end)
        end
    elseif waveChoice == 2 then
        getShootWave = function()
            local shootAnimRate = 0.15
            return Timers.create(shootAnimRate):andThen(function()
                shootFunc(6, 0)
            end):thenWait(shootAnimRate):andThen(function()
                shootFunc(6, 1/3)
            end):thenWait(shootAnimRate):andThen(function()
                shootFunc(6, 2/3)
            end)
        end
    else -- waveChoice == 3
        getShootWave = function()
            local shootAnimRate = 0.15
            return Timers.create(shootAnimRate):andThen(function()
                shootFunc(6,0)
            end):thenWait(shootAnimRate):andThen(function()
                shootFunc(6,2/3)
            end):thenWait(shootAnimRate):andThen(function()
                shootFunc(6,1/3)
            end)
        end
    end

    -- 3 waves
    local wave = getShootWave()
    local waveCount = 1 + math.random(4)
    for i=2,waveCount do
        wave = wave:append(getShootWave())
    end

    self.anim:append(wave):thenWait(1+math.random()):andThen(function()
        -- new barrage or switch behaviour
        if math.random() < 0.5 then
            Boss.setState(Boss_SpreadState)
        else
            Boss.setState(Boss_InverseRamState)
        end
    end)


end

function Boss_SpreadState.leave(self)
    if self.anim then self.anim:cancel() end
end
