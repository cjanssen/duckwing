
---------------------------------------------------
Boss_HomingBallState = Boss_HomingBallState or {}
function Boss_HomingBallState.launch(self)
    -- INIT
    self.wandering = false
    self.freq = 0.2 * Vector(1,3.5)
    self.wanderSize = Vector(200,32)
    self.wanderTopLeft = (Vector(500,128+32) - self.wanderSize)*0.5

    do
        local frac = Boss.pos.x / (self.wanderSize.x + self.wanderTopLeft.x)
        local as = math.asin(2*frac-1)/(2*math.pi*self.freq.x)
        if type(as) == "number" and as>0 and as<math.huge then
            self.time = as
        else
            self.time = 0
        end
    end

    do
        local frac = Vector(
                0.5 * math.sin(2*math.pi*self.time * self.freq.x) + 0.5,
                0.5 * math.sin(2*math.pi*self.time * self.freq.y) + 0.5)


        self.initialDest = frac ^ self.wanderSize + self.wanderTopLeft
    end


    self.anim = Boss.createResetAnim(self.initialDest, 0.3, 300)
    :andThen(function()
        self.wandering = true
    end)


    local wanderTime = math.random()*6 + 4
    local lifeAnim = Timers.create(wanderTime):andThen(function()
        Boss.setState(Boss_SniperState)
        end)

    local firingRate = 0.78
    local fireAnim = Timers.create(firingRate)
        :andThen(function()
            sfxBossShootHoming()
            Bullets.addEnemyHomingBullet(Boss.pos + Vector(0,-18), math.random()*math.pi*2)
        end):thenRestart()

    self.anim:append(lifeAnim)
    self.anim:append(fireAnim)
    -- fire a bullet immediately when reached startpos
    self.anim:andThen(function()
            sfxBossShootHoming()
            Bullets.addEnemyHomingBullet(Boss.pos + Vector(0,-18), math.random()*math.pi*2)
        end)
end

function Boss_HomingBallState.update(self,dt)
    -- Boss_WanderState
    if self.wandering then
        self.time = self.time + dt
        local frac = Vector(
                    0.5 * math.sin(2*math.pi*self.time * self.freq.x) + 0.5,
                    0.5 * math.sin(2*math.pi*self.time * self.freq.y) + 0.5)
        -- frac = frac * 0.5 + Vector(0.5,0.5)
        -- Boss.pos = frac ^ screenSize
        -- Boss.pos = (self.areaBR-self.areaTL) ^ frac + self.areaTL
        Boss.pos = frac ^ self.wanderSize + self.wanderTopLeft
    end
end

function Boss_HomingBallState.leave(self)
    Bullets.scheduleKillAllEnemyBullets()
end
