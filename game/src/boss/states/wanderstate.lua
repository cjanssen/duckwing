
------------------------------------------------------------------------------
Boss_WanderState = Boss_WanderState or {}

function Boss_WanderState.reset(self)
    self.time = 0
end

function Boss_WanderState.launch(self)
    self.time = self.time or 0
    self.freq = 0.25 * Vector(1,5.5)
    -- self.areaTL = Vector(32,0)
    -- self.areaBR = Vector(300-32,64)
    self.wanderSize = Vector(200,32)
    self.wanderTopLeft = (Vector(500,128+32) - self.wanderSize)*0.5

    local wanderTime = math.random()*4 + 4
    self.anim = Timers.create(wanderTime):andThen(function() Boss.setState(Boss_RamState) end)

    local firingRate = 0.6
    Timers.create(firingRate)
        :andThen(function()
            Bullets.addEnemyDirBullet(Boss.pos + Vector(-30,24), 4 * math.pi/6)
            Bullets.addEnemyDirBullet(Boss.pos + Vector(30,24), 2 * math.pi/6)
            sfxBossBigBullet()
        end):loopObserve(self.anim):start()
end

function Boss_WanderState.update(self, dt)
    -- Boss_WanderState
    self.time = self.time + dt
    local frac = Vector(
                0.5 * math.sin(2*math.pi*self.time * self.freq.x) + 0.5,
                0.5 * math.sin(2*math.pi*self.time * self.freq.y) + 0.5)
    -- frac = frac * 0.5 + Vector(0.5,0.5)
    -- Boss.pos = frac ^ screenSize
    -- Boss.pos = (self.areaBR-self.areaTL) ^ frac + self.areaTL
    Boss.pos = frac ^ self.wanderSize + self.wanderTopLeft
end
