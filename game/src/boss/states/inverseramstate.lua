
---------------------------------------------------
Boss_InverseRamState = Boss_InverseRamState or {}
function Boss_InverseRamState.launch(self)
    self.anim = Boss.createResetAnim(Vector(Boss.pos.x, -64), 1, 100)

    function getRamAnim()
        local ramDelay = 0.7
        local ramOrig = screenSize.y+64
        local ramDist = -screenSize.y-128
        return Timers.create(math.random()*2 + 1)
            :thenWait(ramDelay)
            :prepare(function()
                local ramSpeed = ramDist/ramDelay
                local meetTime = (ramOrig-Player.pos.y)/(Player.vel.y*Player.speed*1.25-ramSpeed)
                local meetPoint = Player.pos.x + randex(0.85) * Player.vel.x * Player.speed * meetTime
                Boss.pos = Vector(math.clamp(64,meetPoint,mapSize.x-64), ramOrig)
                Boss.flipped = true
                sfxBossInverseRam()
                end)
            :withUpdate(function(elapsed)
                Boss.pos.y = ramOrig + ramDist * elapsed/ramDelay
                end)
            :thenWait(math.random())
    end

    local finalPos = Vector(250, 64)
    local storedPos, returnTime, returnDist
    local returnAnim = Timers.create()
        :prepare(function(timer)
            storedPos = Boss.pos:copy()
            returnDist = finalPos - storedPos
            returnTime = math.max(1, returnDist:mod() / 100)
            timer:withTimeout(returnTime)
            Boss.flipped = false
            end)
        :withUpdate(function(elapsed)
            Boss.pos = storedPos + elapsed/returnTime * returnDist
        end)

    self.anim
        :append(getRamAnim())
        :append(getRamAnim())
        :append(getRamAnim())
        :append(returnAnim)
        :andThen(function()
        Boss.setState(Boss_SpreadState)
    end)
end
