Boss_HurtState = Boss_HurtState or {}
function Boss_HurtState.launch(self)
    local preWait = 1
    local vibrateTime = 1
    local interim = 0.5
    local restTime = 1.5
    local vibrateAmp = 8

    local vibrateAnim = Timers.create(vibrateTime):withUpdate(function()
        Boss.bottomOfs = Vector(math.random()-0.5,math.random()-0.5) * vibrateAmp
        end)
        :andThen(function()
            Boss.bottomOfs = Vector()
        end)
        :thenWait(interim)

    local finalWaitTime = 1
    self.anim = Timers.create(preWait):prepare(function()
        Boss.vulnerable = false
        end)
    :append(vibrateAnim)
    :thenWait(restTime)
    :andThen(function()
        Boss.setState(self.nextState)
    end)
    :finally(function()
        Boss.vulnerable = true
        Boss.flipped = false
        Boss.bottomOfs = Vector()
    end)

    if Boss.lives > 0 then
        local liveDelay = preWait+restTime+vibrateTime
        Timers.create(liveDelay):prepare(function()
            Boss.lives_display = 0
            sfxBossChargeLife()
        end):withUpdate(function(elapsed)
            Boss.lives_display = math.ceil(elapsed/liveDelay * Boss.liveStageChange)
        end):andThen(function()
            Boss.lives_display = Boss.liveStageChange
            Boss.lives = math.ceil(Boss.lives/Boss.liveStageChange)*Boss.liveStageChange
        end)
        :start()
    end

    Timers.create():prepare(function()
        Explo.addSmall(Boss.pos + Vector(math.random()-0.5,math.random()-0.5)^Boss.size)
        sfxBossStageHit()
    end):andThen(function(timer)
        timer:withTimeout(math.random()*0.5+0.1)
    end):loopObserve(self.anim)
    :start()

end

function Boss_HurtState.setup(self, nextState)
    self.nextState = nextState
end
