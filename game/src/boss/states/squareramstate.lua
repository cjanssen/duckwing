
---------------------------------------------------------
Boss_SquareRamState = Boss_SquareRamState or {}

function Boss_SquareRamState.launch(self)
    local startPos = Vector(Boss.pos.x, 64)
    self.anim = Boss.createResetAnim(startPos)

    local getStage = function(to, speed)
        local from,dist,duration
        return Timers.create(100)
        :prepare(function(timer)
            from = Boss.pos:copy()
            dist = to - from
            duration = dist:mod()/speed
            timer:withTimeout(duration)
        end)
        :withUpdate(function(elapsed)
            Boss.pos = from + dist * elapsed / duration
        end)
        :andThen(function()
            Boss.pos = to:copy()
        end)
    end

    local s = randomSign()
    local getRamLoop = function()
        s = -s
        local getRP = function() return math.clamp(64, (s * (64 + randex(96)) + 500) % 500, 500-64) end
        local horzSpeed = 150
        local downSpeed = 350
        local upSpeed = 200
        local ramTL = Vector(getRP(), 64)
        local ramBR = Vector(getRP(), 200-48)
        ramBR.x = 500-ramBR.x -- force symmetric
        local ramBL = Vector(ramTL.x, ramBR.y)
        local ramTR = Vector(ramBR.x, ramTL.y)

        local fixedPos
        local chargeAnim = Timers:create(0.5):prepare(function()
                fixedPos = Boss.pos:copy()
            end)
            :withUpdate(function()
                Boss.pos = fixedPos + Vector((math.random()-0.5)*4,0)
            end):andThen(function()
                Boss.pos = fixedPos:copy()
            end)
        return Timers.create()
            :hang(getStage(ramTL, horzSpeed))
            :hang(chargeAnim:fork())
            :hang(getStage(ramBL, downSpeed):prepare(sfxBossRam))
            :hang(getStage(ramBR, horzSpeed))
            :hang(getStage(ramTR, upSpeed):prepare(sfxBossRam))
    end

    -- loop up to 3 times
    local a = self.anim:append(getRamLoop())
    if math.random()<0.5 then
        a = a:append(getRamLoop())
        if math.random()<0.5 then
            a = a:append(getRamLoop())
        end
    end

    a:andThen(function()
        if math.random()<0.5 then
            Boss.setState(Boss_HelicopterState)
        else
            Boss.setState(Boss_ZigZagSpreadState)
        end
    end)

    local shootDelay = 1
    Timers.create(shootDelay):andThen(function()
        sfxBossBigBullet()
        Bullets.addEnemyDirBullet(Boss.pos, (Player.pos - Boss.pos):angle())
    end):loopObserve(self.anim):start()

end

