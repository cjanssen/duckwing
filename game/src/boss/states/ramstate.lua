
---------------------------------------------------------
-- Bullets.addEnemyDirBullet(startPos, startAngle)
Boss_RamState = Boss_RamState or {}

function Boss_RamState.launch(self)
    local freezePos = Boss.pos:copy()
    local freezeTime = 0.6 --0.5
    local ramTime = 0.3
    local retrieveTime = 1
    local ramAmount = 150
    self.anim = Timers.create(freezeTime):withUpdate(function(elapsed)
            Boss.pos.x = freezePos.x + (math.random()-0.5)*5
            end)
        :andThen(function() Boss.pos = freezePos:copy()
                sfxBossRam()
            end)
        :thenWait(ramTime):withUpdate(function(elapsed)
                Boss.pos.y = freezePos.y + elapsed / ramTime * ramAmount
            end)
        :thenWait(retrieveTime):withUpdate(function(elapsed)
                Boss.pos.y = freezePos.y + (1 - elapsed / retrieveTime) * ramAmount
            end)
        :andThen(function()
            Boss.setState(Boss_WanderState)
            Boss.pos = freezePos:copy()
            end)
end
