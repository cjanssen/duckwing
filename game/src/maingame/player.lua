Player = Player or {}

function startPlayer()
	local props = {
		pos = Vector(screenSize.x/2, screenSize.y/4*3),
		speed = 2250,
		drag = 0.55,
		dir=Vector(0,0),
		acc = 2.5,
		vel=Vector(0,0),
		size = Vector(8,8),

		shootTimer = 0,
		shootDelay = 0.15,

		alive = true,



		immortal = false,
		transparent = false,
		spawning = false,
		canshoot = true,

		dashing = false,
		dashQueue = {},

		lives = 15,

		hitFrameCount = 0,
		blinking = false,
	}

	local borders = Box(40,8,36,16)
	Player.moveLimit = Box(borders.x0, borders.y0, mapSize.x - borders.x1, mapSize.y - borders.y1)

	for k,v in pairs(props) do
		Player[k] = v
	end
end

function Player.hide()
	Player.alive = false
end

function Player.spawn()
	Player.killAnims()

	Player.spawning = true
	Player.alive = true

	adjustScroll(Player.pos)
	local spawnDelay = 1
	local immortalDelay = 1.5
	local spawnDest = Player.pos:copy()
	spawnDest.y = screenSize.y - 32

	local spawnLimit = Vector(1, math.min(spawnDest.y*math.sqrt(2)/(spawnDest.x+1), 1))
	local spawnFrameDelay = 0.03
	Player.spawnFrame = 1
	local blinkFrameFreq = 0.03

	Player.pos = Vector(0,spawnDest.y)

	if Player.spawnAnim then Player.spawnAnim:cancel() end
	Player.spawnAnim = Timers.create()
	Player.spawnAnim:thenWait(spawnDelay):withUpdate(function(elapsed)
			local frac = elapsed / spawnDelay
			local spawnAngle = -math.pi + math.pi * frac
			local spawnRadius = (1-frac) * spawnDest.x
			Player.pos = spawnDest + VectorFromPolar(spawnRadius, spawnAngle) ^ spawnLimit
			Player.spawnFrame = math.floor(elapsed / spawnFrameDelay) % #Player.spawnFrames + 1
		end)
		:andThen(function()
			Player.spawning = false
			-- Player.canshoot = false
			Player.transparent = true
			sfxPlayerSpawn()
		end)
		:thenWait(immortalDelay)
		:withUpdate(function(elapsed)
			Player.blinking = math.floor(elapsed/blinkFrameFreq) % 3 == 1
			end)
		:finally(function()
			Player.transparent = false
			Player.blinking = false
			Player.canshoot = true
			Player.spawning = false
		end)


	Player.spawnAnim:start()
end

function Player.killAnims()
	if Player.spawnAnim then Player.spawnAnim:cancel() end
	if Player.dashAnim then Player.dashAnim:cancel() end
	if Player.dashInputTimer then Player.dashInputTimer:cancel() end
	Player.transparent = false
	Player.blinking = false
	Player.canshoot = true
	Player.spawning = false
	Player.dashing = false
	Player.postDash = false
end

function Player.dash(dir)
	Player.killAnims()
	Player.dashing = true
	local dashOrig = Player.pos:copy()
	local dashDist = 200
	if math.abs(dir.y) > math.abs(dir.x) then
		-- vertical dash: shorter because lack of scroll
		dashDist = 115
	end
	Player.dashMirror = dir.x < 0
	Player.dashFrame = 1
	local lastFrame = 1
	Player.postDash = false
	local frameDelay = 0.016
	local dashLoopDelay = frameDelay * 12
	local dashRainbowDelay = dashLoopDelay * 2
	local dashDelay = dashLoopDelay * 3

	if Player.dashAnim then Player.dashAnim:cancel() end
	Player.dashAnim = Timers.create()
	Player.dashAnim:thenWait(dashDelay):withUpdate(function(elapsed)
		Player.dashFrame = math.floor(elapsed/frameDelay) % #Player.dashFrames + 1
	end)
	Player.dashAnim:thenWait(dashRainbowDelay):withUpdate(function(elapsed)
		Player.pos = dashOrig + dir*dashDist*elapsed/dashRainbowDelay
		Player.pos = Player.pos:clamp(Player.moveLimit:topLeft(), Player.moveLimit:bottomRight())
		end):andThen(function()
			Player.dashing = false
			Player.postDash = true
		end):thenWait(dashLoopDelay):andThen(function()
			Player.postDash = false
		end):finally(function()
			Player.dashing = false
			Player.postDash = false
			Player.dashQueue = {}
		end)

	-- queueAnim
	local queueAnim = Timers.create():observe(Player.dashAnim):withUpdate(function()
		if lastFrame ~= Player.dashFrame then
			lastFrame = Player.dashFrame
			-- by default, make queue empty
			local dashQueueLimit = 0
			if Player.dashing then
				table.insert(Player.dashQueue, {
					sprite = Player.dashFrames[Player.dashFrame],
					pos = Player.pos:floor(),
					mirror = Player.dashMirror and -1 or 1
					})
				-- if dashing, let it grow to 3 frames
				dashQueueLimit = 3
			end

			-- unwind queue
			if #Player.dashQueue>dashQueueLimit then
				table.remove(Player.dashQueue, 1)
			end
		end
	end):start()

	Player.dashAnim:start()
	sfxPlayerDash()
end

function initPlayer()
	Player.prepareNewFrames()
end

function Player.prepareNewFrames()
	local img = love.graphics.newImage("img/shipsheet.png")
	local w,h = img:getWidth(), img:getHeight()

	-- normal move
	Player.frames = {}
	Player.frameSize = Vector(64,64)
	local positions = {
		{ x = 64, y = 6*64 },
		{ x = 0,  y = 6*64 },
		{ x = 0,  y = 7*64 },
		{ x = 0,  y = 5*64 },
		{ x = 64, y = 5*64 },
	}
	Player.batch = love.graphics.newSpriteBatch(img, 10)
	for i,position in ipairs(positions) do
		Player.frames[i] = love.graphics.newQuad(position.x, position.y, 64, 64, w, h)
	end

	Player.dirFrame = 3
	Player.dirFrameTime = 0
	Player.dirFrameDelay = 0.05

	-- spawn animation
	Player.spawnFrames = {}

	for i=0,11 do
		table.insert(Player.spawnFrames,
			love.graphics.newQuad(i*64, 7*64, 64, 64, w, h))
	end

	-- dash animation
	Player.dashFrames = {}
	for i=0,11 do
		table.insert(Player.dashFrames,
			love.graphics.newQuad(i*64, 1*64, 64, 64, w, h))
	end

end

function Player.update(dt)
	if not Player.alive then return end

	-- superinertia pickup
	Player.drag = 0.55

	Player.evalKeys()

	Player.move(dt)
	Player.updateShotTimer(dt)
	Player.checkBullets()
	Player.checkEnemyCollision()
	Player.updateFrames(dt)
end

function Player.updateFrames(dt)
	local p = Player

	if Player.dirFrameTime > 0 then
		Player.dirFrameTime = Player.dirFrameTime - dt
	end


	if Player.dirFrameTime <= 0 then
		Player.dirFrameTime = Player.dirFrameDelay

		local destFrame = 3
		if p.dir.x < 0 then destFrame = 1 end
		if p.dir.x > 0 then destFrame = 5 end
		if Player.dirFrame ~= destFrame then
			Player.dirFrame = Player.dirFrame + math.sign(destFrame - Player.dirFrame)
		end
	end
end

function Player.updateShotTimer(dt)
	if Player.shootTimer > 0 then
		Player.shootTimer = Player.shootTimer - dt
	end
	Player.adjustShootTimer()
end

function Player.adjustShootTimer()
	if Player.shootTimer > Player.shootDelay then
		Player.shootTimer = Player.shootDelay
	end
end

function Player.move(dt)
	local p = Player
	local drag = math.pow(p.drag,dt*60)
	p.vel = p.vel * drag + p.dir * p.acc * dt
	p.pos = p.pos + (p.vel * p.speed * dt)

	if not Player.spawning then
		p.pos = p.pos:clamp(Player.moveLimit:topLeft(), Player.moveLimit:bottomRight())
	end
end

function Player.draw()
	local p = Player
	if not p.alive then return end

	love.graphics.push()
	love.graphics.translate((-Scroll.layers.top):floor():unpack())
	love.graphics.setColor(255,255,255)

	Player.batch:clear()

	-- draw dash queue
	for i=1,#Player.dashQueue do
		local q = Player.dashQueue[i]
		love.graphics.draw(Player.batch:getTexture(), q.sprite,
			q.pos.x, q.pos.y, 0, q.mirror, 1, Player.frameSize.x*0.5, Player.frameSize.y*0.5)
	end


	local pos = p.pos:floor()
	local mirror = 1
	local sprite
	if p.spawning then
		sprite = Player.spawnFrames[Player.spawnFrame]
	elseif p.dashing then
		sprite = Player.dashFrames[Player.dashFrame]
		if Player.dashMirror then mirror = -1 end
	elseif p.postDash then
		sprite = Player.spawnFrames[Player.dashFrame]
		-- the spawnframes rotate in the oposite direction, thus negating the mirror
		if not Player.dashMirror then mirror = -1 end
	else
		sprite = Player.frames[Player.dirFrame]
	end

	if p.blinking then
		Shaders.enableWhiteSpriteShader()
	end

	love.graphics.draw(Player.batch:getTexture(), sprite,
		pos.x, pos.y, 0, mirror, 1, Player.frameSize.x*0.5, Player.frameSize.y*0.5)


	if p.blinking then
		Shaders.clear()
	end

	love.graphics.pop()
end

function Player.shoot()
	if Player.shootTimer <= 0 then
		Player.shootTimer = Player.shootDelay
		Bullets.addPlayerBullet(Player.pos)
		sfxPlayerShoot()
	end
end

function Player.kill()
	if not Player.immortal then
		Player.alive = false
		-- registerPlayerDied()
		Player.lives = Player.lives - 1

		local hitTime = 0.03
    	Timers.create(hitTime):prepare(function()
        	Player.hitFrameCount = Player.hitFrameCount + 1
        end)
        :andThen(function()
            Player.hitFrameCount = Player.hitFrameCount - 1
            Explo.add(Player.pos)
            if Player.lives > 0 then
				Player.spawn()
			else
				SpecialMode.setState("gameover")
			end
        end):start()

        sfxPlayerHit()

        -- if Player.lives <= 0 then
        -- 	Timers.create(1):andThen(function()
        -- 		SpecialMode.setState("gameover")
        -- 	end):start()
        -- end
	end
end

function Player.vulnerable()
	return Player.alive and not Player.spawning and not Player.dashing and not Player.transparent
end

function Player.checkBullets()
	for i,v in ipairs(Bullets.enemyList) do
		if not v.dead and not v.invisible and Player.vulnerable() and checkBoxes(v, Player) then
			Player.kill()
			if not v.solid then
				Bullets.scheduleKill(Bullets.enemyList,i,Player)
			end
		end
	end
end

function Player.checkEnemyCollision()
	if Boss.visible and Player.vulnerable() and checkBoxes(Player, Boss) then
		Player.kill()
	end
end

function Player.getInput(inputNumber)
	if inputNumber == 1 then
		-- up
		return love.keyboard.isDown("w") or love.keyboard.isDown("up")

	elseif inputNumber == 2 then
		-- down
		return love.keyboard.isDown("s") or love.keyboard.isDown("down")
	elseif inputNumber == 3 then
		-- left
		return love.keyboard.isDown("a") or love.keyboard.isDown("left")
	elseif inputNumber == 4 then
		-- right
		return love.keyboard.isDown("right") or love.keyboard.isDown("d")
	elseif inputNumber == 5 then
		-- fire
		return love.keyboard.isDown("lctrl") or love.keyboard.isDown("lshift")
			or love.keyboard.isDown("rctrl") or love.keyboard.isDown("rshift")
			or love.keyboard.isDown("space")
	end
end

function Player.evalKeys()
	if Player.spawning or Player.dashing then
		return
	end

	local olddir = Player.dir:copy()
	local dir = Player.dir:copy()

	dir.x = 0
	if Player.getInput(3) then
		dir.x = -1
	end

	if Player.getInput(4) then
		dir.x = 1
	end

	dir.y = 0
	if Player.getInput(1) then
		dir.y = -1
	end

	if Player.getInput(2) then
		dir.y = 1
	end

	if joystickOpen then
		dx = joystickOpen:getAxis(1)
		dy = joystickOpen:getAxis(2)
		if joystickOpen:isGamepad() then
			if joystickOpen:isGamepadDown("dpup") then dy = -1 end
			if joystickOpen:isGamepadDown("dpdown") then dy = 1 end
			if joystickOpen:isGamepadDown("dpleft") then dx = -1 end
			if joystickOpen:isGamepadDown("dpright") then dx = 1 end
		end
	    if math.abs(dx)<0.5 then dx = 0 end
	    if math.abs(dy)<0.5 then dy = 0 end

		if dx ~= 0 or dy~= 0 then
			dir.x = dx
			dir.y = dy
		end
	end

	Player.dir = dir:norm()

	-- check dashing
	-- first, check if key/input pressed
	if olddir:isZero() and not dir:isZero() then
		-- if there was an old keypress
		local wasRunning = false
		local newdir = VectorFromAngle(math.floor(0.5 + dir:angle()*2/math.pi) * math.pi*0.5)
		local olddir = -newdir

		if Player.dashInputTimer then
			-- stop the old timer
			wasRunning = Player.dashInputTimer:isRunning()
			Player.dashInputTimer:cancel()
			olddir = Player.dashInputTimer:getData().dir
		end

		-- if keypress within range
		if wasRunning and newdir:isEq(olddir) then
			-- dash!
			Player.dash(newdir)
		else
			-- register keypress as timer
			local dashInputDelay = 0.225
			Player.dashInputTimer = Timers.create(dashInputDelay):withData({dir = newdir}):start()
		end
	end

	-- check shooting
	if Player.canshoot then
		local doShoot = false
		if Player.getInput(5) then
			doShoot = true
		end

		if joystickOpen then
			if joystickOpen:isGamepad() then
				doShoot =  doShoot or joystickOpen:isGamepadDown("x","y","a","b","leftstick","rightstick","leftshoulder","rightshoulder")
			elseif joystickSpecial then
				local nb = joystickOpen:getButtonCount()
				for i=0,nb-1 do
					if joystickOpen:isDown(i) and (i~=9 and i~=10) then
						doShoot = true
						break
					end
				end
			else
				local nb = joystickOpen:getButtonCount()
				for i=0,nb-1 do
					if joystickOpen:isDown(i) and ((nb>14 and i~=3 and i~=4) or (nb<15 and (i < 4 or i > 10))) then
						doShoot = true
						break
					end
				end
			end
		end

		if doShoot then
			Player.adjustShootTimer()
			Player.shoot()
		end
	end
end
