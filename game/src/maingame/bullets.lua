Bullets = Bullets or {}

function initBullets()
	Bullets.loadPics()
end

function startBullets()
	Bullets.playerList = {}
	Bullets.enemyList = {}
	Bullets.homingCount = 0
	Bullets.solidCount = 0
	Bullets.max_queue_len = 8

end

function Bullets.wipe()
	startBullets()
end

function Bullets.addPlayerBullet(startPos)
	local ang1 = 15*math.pi/180 - Player.vel.y * 1.05
	if ang1 < 0 then ang1 = 0 end
	local ang2 =  - Player.vel.y * 0.9
	if ang2 < 0 then ang2 = 0 end

	local bulletSpeed = 500
	table.insert(Bullets.playerList, {
		pos = Vector(startPos.x - 4, startPos.y),
		speed = bulletSpeed,
		dir = VectorFromAngle(-math.pi/2 - ang1),
		size = Vector(6,24),
		angle = - ang1
	})
	table.insert(Bullets.playerList, {
		pos = Vector(startPos.x - 4, startPos.y),
		speed = bulletSpeed,
		dir = VectorFromAngle(-math.pi/2 - ang2),
		size = Vector(6,24),
		angle = - ang2
	})
	table.insert(Bullets.playerList, {
		pos = Vector(startPos.x + 4, startPos.y),
		speed = bulletSpeed,
		dir = VectorFromAngle(-math.pi/2 + ang2),
		size = Vector(6,24),
		angle = ang2
	})
	table.insert(Bullets.playerList, {
		pos = Vector(startPos.x + 4, startPos.y),
		speed = bulletSpeed,
		dir = VectorFromAngle(-math.pi/2 + ang1),
		size = Vector(6,24),
		angle = ang1
	})
end



function Bullets.addEnemyBullet(startPos)
	local bullet = {
		pos = startPos:copy(),
		speed = 300,
		dir = Vector(0,1),
		size = Vector(6,6),
		scale = Vector(math.sign(math.random()-0.5),1),
		angle = 0,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04,
	}
	table.insert(Bullets.enemyList, bullet)

end

function Bullets.addEnemyDirBullet(startPos, startAngle)
	table.insert(Bullets.enemyList, {
		pos = startPos:copy(),
		speed = 200,
		dir = VectorFromAngle(startAngle),
		size = Vector(16,16),
		scale = Vector(math.sign(math.random()-0.5),1),
		angle = 0,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04,
		row = math.random(2) + 2
	})
end

function Bullets.addEnemyDirBulletSmall(startPos, startAngle)
	table.insert(Bullets.enemyList, {
		pos = startPos:copy(),
		speed = 150,
		dir = VectorFromAngle(startAngle),
		size = Vector(8,8),
		scale = Vector(math.sign(math.random()-0.5), 1),
		angle = 0,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04,
		row = math.random(2)
	})
end

function Bullets.addEnemyHomingBullet(startPos, startAngle)
	if Bullets.homingCount < 11 then
		Bullets.homingCount = Bullets.homingCount + 1
		table.insert(Bullets.enemyList, {
			pos = startPos:copy(),
			speed = 400,
			dir = VectorFromAngle(startAngle),
			size = Vector(16,16),
			scale = Vector(math.sign(math.random()-0.5),1),
			angle = 0,
			animated = true,
			frameIdx = 1,
			frameTimer = 0,
			frameDelay = 0.04,
			homing = true,
			row = math.random(2) + 2,
			queue = {},
			queueTimer = 0.03,
			queueDelay = 0.03,
			bouncy = true,
			explodes = true,
		})
	end
end

function Bullets.addEnemyStaticBullet(startPos, scaledSize)
	Bullets.solidCount = Bullets.solidCount + 1
	local newBullet = {
		pos = startPos:copy(),
		speed = 0,
		dir = Vector(),
		size = Vector(16,16) * scaledSize,
		scale = Vector(math.sign(math.random()-0.5),1),
		angle = 0,
		animated = true,
		frameIdx = 1,
		frameTimer = 0,
		frameDelay = 0.04,
		solid = true,
		canExitScreen = true,
		row = math.random(2) + (scaledSize > 0.75 and 2 or 0)
	}
	table.insert(Bullets.enemyList, newBullet)
	return newBullet
end

function Bullets.loadPics()
	local img = love.graphics.newImage("img/shots.png")
	Bullets.batch = love.graphics.newSpriteBatch(img, 2000, "static")
	Bullets.enemyFrames = {}
	for y=1,4 do
		Bullets.enemyFrames[y] = {}
		for x=0,2 do
			Bullets.enemyFrames[y][x+1] =
				love.graphics.newQuad(x*32, y*32, 32, 32, img:getWidth(), img:getHeight())
		end
	end

	Bullets.playerFrame = love.graphics.newQuad(0,160,32,32,img:getWidth(), img:getHeight())
end

function Bullets.scheduleKill(list, num, silent)
	if silent then
		list[num].explodes = false
	end
	list[num].dead = true
end

function Bullets.purge()
	function purgeList(list)
		local n = table.getn(list)
		for i=1,n do
			if list[n+1-i].dead then
				Bullets.kill(list, n+1-i)
			end
		end
	end
	purgeList(Bullets.playerList)
	purgeList(Bullets.enemyList)
	Bullets.killedSound = false
end

function Bullets.kill(list, num)
	local bullet = list[num]
	if bullet.explodes then
		if not Bullets.killedSound then
			Bullets.killedSound = true
			sfxHomingExplode()
		end
		Explo.addSmall(bullet.pos)
	end
	if bullet.homing then
		Bullets.homingCount = Bullets.homingCount - 1
	end
	if bullet.solid then
		Bullets.solidCount = Bullets.solidCount - 1
	end
	table.remove(list, num)
end

function Bullets.update(dt)
	Bullets.purge()
	Bullets.updatePlayerBullets(dt)
	Bullets.updateEnemyBullets(dt)
end

function Bullets.scheduleKillAllEnemyBullets()
	for i,v in ipairs(Bullets.enemyList) do
		Bullets.scheduleKill(Bullets.enemyList, i, false)
	end
end

function Bullets.updatePlayerBullets(dt)
	for i,v in ipairs(Bullets.playerList) do
		v.pos = v.pos + v.dir * v.speed * dt

		if v.canExitOnSides then
			if outOfScreenTops(v.pos, v.size.x/2, v.size.y/2) then
				Bullets.scheduleKill(Bullets.playerList, i, true)
			end
		else
			if outOfScreen(v.pos, v.size.x/2, v.size.y/2) then
				Bullets.scheduleKill(Bullets.playerList, i, true)
			end
		end

		if Bullets.homingCount > 0 then
			for j,enB in ipairs(Bullets.enemyList) do
				if enB.homing and not enB.invisible and checkBoxes(v, enB) then
					Bullets.scheduleKill(Bullets.playerList, i, true)
					Bullets.scheduleKill(Bullets.enemyList, j, false)
				end
			end
		end

		if Bullets.solidCount > 0 then
			for j,enB in ipairs(Bullets.enemyList) do
				if enB.solid and not enB.invisible and checkBoxes(v, enB) then
					Bullets.scheduleKill(Bullets.playerList, i, true)
				end
			end
		end
	end
end

function Bullets.bounceBullet(dt, v, oldpos)
	local l = v.size*0.5
	l.x = l.x*4
	if v.pos.x < l.x then
		local newFrom = 2*l.x - oldpos.x
		v.dir.x = -v.dir.x
		v.pos.x = newFrom + v.dir.x * v.speed*dt
	end
	if v.pos.x > mapSize.x-l.x then
		v.pos.x = 2 * (mapSize.x-l.x) - oldpos.x
		v.dir.x = -v.dir.x
		v.pos.x = v.pos.x + v.dir.x * v.speed*dt
	end
	-- special: do not bounce on top
	-- if v.pos.y < l.y then
	-- 	v.pos.y = 2*l.y - oldpos.y
	-- 	v.dir.y = -v.dir.y
	-- 	v.pos.y = v.pos.y + v.dir.y * v.speed*dt
	-- end
	if v.pos.y > mapSize.y-l.y then
		v.pos.y = 2 * (mapSize.y-l.y) - oldpos.y
		v.dir.y = -v.dir.y
		v.pos.y = v.pos.y + v.dir.y * v.speed*dt
	end
end

function Bullets.updateEnemyBullets(dt)
	for i,v in ipairs(Bullets.enemyList) do
		if not v.invisible then
			-- position
			local oldpos = v.pos:copy()
			v.pos = v.pos + v.dir * v.speed*dt


			if v.bouncy then
				Bullets.bounceBullet(dt, v, oldpos)
			elseif not v.canExitScreen and outOfScreen(v.pos, v.size.x/2, v.size.y/2) then
				Bullets.scheduleKill(Bullets.enemyList, i, true)
			end

			-- homing
			if Bullets.homingCount > 0 and v.homing then
				local dirToPlayer = Player.pos - v.pos
				local ang = v.dir:angleBetween(dirToPlayer)
				local maxTurn = math.pi * 0.6 * dt
				if math.abs(ang) > maxTurn then ang = math.sign(ang)*maxTurn end
				v.dir = v.dir:rot(ang)
				local speedChange = 400 * dt
				local speedDiff = (360-dirToPlayer:mod())*0.75 - v.speed
				if math.abs(speedDiff) > speedChange then
					speedDiff = math.sign(speedDiff) * speedChange
				end
				v.speed = v.speed + speedDiff

				v.queueTimer = v.queueTimer - dt
				if v.queueTimer <= 0 then
					v.queueTimer = v.queueDelay
					table.insert(v.queue,v.pos:copy())
					if #v.queue > Bullets.max_queue_len then
						table.remove(v.queue,1)
					end
				end
			end

			-- animation
			if v.animated and v.frameTimer then
				v.frameTimer = v.frameTimer - dt
				if v.frameTimer <= 0 then
					v.frameTimer = v.frameDelay
					v.frameIdx = (v.frameIdx % table.getn(Bullets.enemyFrames[1])) + 1
				end
			end
		end
	end
end


function Bullets.draw()
	love.graphics.push()
	love.graphics.translate((-Scroll.layers.top):floor():unpack())
	love.graphics.setColor(255,255,255)
	Bullets.batch:clear()
	for i,v in ipairs(Bullets.playerList) do
		local pos = v.pos:floor()
		Bullets.batch:add(Bullets.playerFrame, pos.x, pos.y, v.angle, 1, 1, 16, 16)
	end

	local alpha_factor = 0.84
	local size_factor = 0.88

	for i,v in ipairs(Bullets.enemyList) do
		if not v.invisible then
			local pos = v.pos:floor()

			if v.queue then
				if #v.queue > 0 then
					local sc = v.scale * math.pow(size_factor, #v.queue)
					local alpha = 255 * math.pow(alpha_factor, #v.queue)
					for j=1,#v.queue do
						sc = sc / size_factor
						alpha = alpha / alpha_factor
						Bullets.batch:setColor(255,128 + 127*(j/#v.queue),128,alpha)
						Bullets.batch:add(Bullets.enemyFrames[v.row][v.frameIdx],
							v.queue[j].x, v.queue[j].y, 0, sc.x, sc.y, 16, 16)
					end
				end
				Bullets.batch:setColor(255,255,255)
			end


			Bullets.batch:add(Bullets.enemyFrames[v.row][v.frameIdx],
				pos.x, pos.y, 0, v.scale.x, v.scale.y, 16, 16)
		end
	end
	love.graphics.draw(Bullets.batch)
	love.graphics.pop()
end