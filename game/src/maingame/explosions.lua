Explo = Explo or {}

function startExplo()
	Explo.list = {}
end

function initExplo()
	Explo.initFrames()
end

function Explo.initFrames()
	local img = love.graphics.newImage("img/playerdeath.png")
	Explo.frames = {}
	Explo.batch = love.graphics.newSpriteBatch(img,400,"static")

	for i=0,11 do
		table.insert(Explo.frames, love.graphics.newQuad(i*128, 0, 128,128,img:getWidth(),img:getHeight()))
	end
end

function Explo.add(startPos)
	local delay = 0.04
	table.insert(Explo.list, {
		pos = startPos:copy(),
		frameNdx = 1,
		frameTimer = delay,
		frameDelay = delay,
		speed = -150,
		valid = true,
		})
end

function Explo.addSmall(startPos)
	local delay = 0.04
	table.insert(Explo.list, {
		pos = startPos:copy(),
		frameNdx = 1,
		frameTimer = delay,
		frameDelay = delay,
		speed = -10,
		valid = true,
		specialExplo = true
		})
end

function Explo.update(dt)
	for i,v in ipairs(Explo.list) do
		-- pos
		v.pos.y = v.pos.y + v.speed * dt
		-- frames
		v.frameTimer = v.frameTimer - dt
		if v.frameTimer <= 0 then
			v.frameTimer = v.frameDelay
			v.frameNdx = v.frameNdx + 1
			if v.frameNdx > table.getn(Explo.frames) then
				v.valid = false
			end
		end
	end

	-- purge
	local n = table.getn(Explo.list)
	for i=1,n do
		local v = Explo.list[n-i+1]
		if not v.valid then
			table.remove(Explo.list,n-i+1)
		end
	end
end

function Explo.draw()
	love.graphics.push()
	love.graphics.translate((-Scroll.layers.top):floor():unpack())
	for i,v in ipairs(Explo.list) do
		local pos = v.pos:floor()
		if v.specialExplo then
			Shaders.enableWhiteSpriteShader()
			love.graphics.setColor(64,64,64)
			love.graphics.draw(Explo.batch:getTexture(), Explo.frames[v.frameNdx],
				pos.x, pos.y, 0, 0.4, 0.4, 64, 64)
			Shaders.enableFlipChannelShader("brg")
			love.graphics.setColor(128,128,128,128)
			love.graphics.draw(Explo.batch:getTexture(), Explo.frames[v.frameNdx],
				pos.x, pos.y, 0, 0.4, 0.4, 64, 64)
			Shaders.clear()
		else
			love.graphics.setColor(255,255,255)
			love.graphics.draw(Explo.batch:getTexture(), Explo.frames[v.frameNdx],
				pos.x, pos.y, 0, 1, 1, 64, 64)
		end
	end
	-- Explo.batch:clear()
	-- for i,v in ipairs(Explo.list) do
	--
	-- 	addToBatch(Explo.batch,Explo.frames[v.frameNdx], pos.x, pos.y, 0, 1, 1, 64, 64)
	-- end
	-- love.graphics.draw(Explo.batch)
	love.graphics.pop()
end