Clouds = Clouds or {}

function initClouds()
    Clouds.imgs = {
        love.graphics.newImage("img/cloud1.png"),
        love.graphics.newImage("img/cloud2.png"),
        love.graphics.newImage("img/cloud3.png"),
        love.graphics.newImage("img/cloud4.png"),
    }

    Clouds.list = {}
    Clouds.spriteSize = Vector(128,128)
    Clouds.speed = 150
    Clouds.timer = 0
end

function resetClouds()
    -- local function getCloudTimer()
    --     return Timers.create(math.random()*1.5 + 0.25):andThen(spawnCloud):start()
    -- end

    -- getCloudTimer():andThen(getCloudTimer)
    Clouds.timer = 0
    Clouds.spawnRate = 4
    -- Clouds.delay = math.random()*1.5 + 0.25
end

function spawnCloud()
    local cloud = {
        pos = Vector(math.random(mapSize.x), -Clouds.spriteSize.y),
        img = Clouds.imgs[math.random(#Clouds.imgs)]
    }

    table.insert(Clouds.list, cloud)
end

function updateClouds(dt)
    Clouds.timer = Clouds.timer - dt
    if Clouds.timer <= 0 then
        -- Clouds.timer = 0
        Clouds.timer = (math.random()*1.5 + 0.25) / Clouds.spawnRate
        spawnCloud()
    end

    for i=#Clouds.list,1,-1 do
        local cloud = Clouds.list[i]
        cloud.pos.y = cloud.pos.y + Clouds.speed * dt
        if cloud.pos.y > screenSize.y + Clouds.spriteSize.y then
            table.remove(Clouds.list, i)
        end
    end

end

function drawClouds()
    love.graphics.setColor(255,255,255)
    love.graphics.push()
    love.graphics.translate((-Scroll.layers.mid):floor():unpack())
    for _,cloud in ipairs(Clouds.list) do
        love.graphics.draw(cloud.img, cloud.pos.x, cloud.pos.y)
    end
    love.graphics.pop()
end