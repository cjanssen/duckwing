function initBg()
	prepareNewBg()
	prepareNewFg()
end

function startBg()
	bgSpeed = { x = 0, y = 100 }
end

function prepareNewBg()
	local img = love.graphics.newImage("img/bg-sprites.png")
	BG = {
		batch = love.graphics.newSpriteBatch(img, 100,"static"),
		frames = {},
		pos = Vector(0,0),
		frame = 1,
		frameDelay = 0.016,
		timer = 0,
		frameSize=Vector(500,200)
	}
	local w,h = img:getWidth(),img:getHeight()

	local spritelist = {{x = 500,y = 1000},
		{x = 1000,y = 200},
		{x = 1500,y = 200},
		{x = 1500,y = 800},
		{x = 1500,y = 1000},
		{x = 1500,y = 1200},
		{x = 0,y = 1400},
		{x = 500,y = 1400},
		{x = 1000,y = 1400},
		{x = 1500,y = 1400},
		{x = 0,y = 0},
		{x = 500,y = 0},
		{x = 500,y = 200},
		{x = 0,y = 400},
		{x = 500,y = 400},
		{x = 0,y = 600},
		{x = 500,y = 600},
		{x = 0,y = 800},
		{x = 500,y = 800},
		{x = 1000,y = 0},
		{x = 1000,y = 400},
		{x = 1000,y = 600},
		{x = 1000,y = 800},
		{x = 0,y = 1000},
		{x = 0,y = 200},
		{x = 1000,y = 1000},
		{x = 0,y = 1200},
		{x = 500,y = 1200},
		{x = 1000,y = 1200},
		{x = 1500,y = 0},
		{x = 1500,y = 400},
		{x = 1500,y = 600},
	}

	for _,coord in ipairs(spritelist) do
		table.insert(BG.frames, love.graphics.newQuad(coord.x,coord.y,BG.frameSize.x,BG.frameSize.y,w,h))
	end
end

function prepareNewFg()
	local img = love.graphics.newImage("img/overlaysheet.png")
	FG = {
		batch = love.graphics.newSpriteBatch(img, 2),
		frames = {},
		frame = 1,
		timer = 0,
		frameDelay = 0.05
	}

	for i=0,1 do
		table.insert(FG.frames, love.graphics.newQuad(i*300,0, 300, 200, 600, 200))
	end
end

function drawBg()
	love.graphics.push()
	love.graphics.translate((-Scroll.layers.back):floor():unpack())
	love.graphics.setColor(255,255,255)
	love.graphics.draw(BG.batch)

	love.graphics.pop()
end

function drawFg()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(FG.batch)
end

function updateBg(dt)
	-- vertical scroll
	BG.pos.y = 0
	BG.timer = (BG.timer + dt) % (BG.frameDelay * #BG.frames)
	BG.frame = math.floor(BG.timer / BG.frameDelay) + 1

	BG.batch:clear()
	BG.batch:add(BG.frames[BG.frame], 0, 0)

	---- FG
	FG.timer = FG.timer + dt
	local oldFrame = FG.frame
	FG.frame = math.floor(FG.timer/FG.frameDelay) % #FG.frames + 1
	if oldFrame ~= FG.frame then
		FG.batch:clear()
		FG.batch:add(FG.frames[FG.frame], 0, 0)
	end
end

