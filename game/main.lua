function love.load(args)
	love.filesystem.load("src/core/constants.lua")()
	love.filesystem.load("src/util/utils.lua")()
	love.filesystem.load("src/maingame/player.lua")()
	love.filesystem.load("src/maingame/bullets.lua")()
	love.filesystem.load("src/maingame/background.lua")()
	love.filesystem.load("src/maingame/explosions.lua")()
	love.filesystem.load("src/util/extras.lua")()
	love.filesystem.load("src/core/sound.lua")()
	love.filesystem.load("src/maingame/clouds.lua")()
	love.filesystem.load("src/core/shaders.lua")()
	love.filesystem.load("src/core/scroll.lua")()
	love.filesystem.load("src/core/text.lua")()
	love.filesystem.load("src/core/specialmode.lua")()
	love.filesystem.load("src/boss/boss.lua")()


	love.graphics.setDefaultFilter( "nearest", "nearest")


	initialize()
	-- start in fullscreen?

	parseArgs(args)

	-- toggleFullscreen()
	newGame()
end

function parseArgs(args)
	local function beginsWith(str, other)
	   return string.sub(str,1,string.len(other))==other
	end

	for i,arg in ipairs(arg) do
		if arg == "-fullscreen" and not love.window.getFullscreen() then
			toggleFullscreen()
		end

		if arg == "-vsync" then
			default_vsync = not default_vsync
		end

		if arg == "-nosound" and Sound.active then
			toggleSound()
		end

		if arg == "-original-resolution" then
			love.window.setMode(300,200)
		end
	end
end

function initialize()
	screenSize = Vector(300, 200)
	default_vsync = false
	-- mapSize = screenSize ^ Vector(1.25, 1)
	mapSize = Vector(500,200)
	screenCanvas = love.graphics.newCanvas(300,200)
	loadFont()
	scale = {1,1}
	translation = {0,0}
	initBg()
	initPlayer()
	initBullets()
	initExplo()
	loadJoystick()
	initSound()
	initClouds()
	initScroll()
	Boss.init()
	SpecialMode.init()
end

function newGame()
	Timers.cancelAll()
	startBg()
	startPlayer()
	Player.spawn()
	startBullets()
	startExplo()
	resetClouds()
	Boss.reset()
	SpecialMode.reset()
	startMusic()
end

function love.joystickadded( joystick )
	loadJoystick()
end

function love.joystickremoved( joystick )
	joystickOpen = false
	loadJoystick()
end

function loadJoystick()
	if not joystickOpen then
		local jc = love.joystick.getJoystickCount()
		if jc == 0 then return end
		local joys = love.joystick.getJoysticks()
		-- try from 0 to 4
		for i,v in ipairs(joys) do
			if v:getButtonCount()>0 then
				joystickOpen = v
				if not joystickOpen:isGamepad() and (string.upper(joystickOpen:getName()) == "XEQX GAMEPAD SL-6556-BK") then
					joystickSpecial = true
				end
				return
			end
		end
		joystickOpen = false
	end
end

function love.draw()
	love.graphics.setCanvas(screenCanvas)
	love.graphics.clear()

	if Player.hitFrameCount > 0 then
		love.graphics.setColor(255,255,255)
		love.graphics.rectangle("fill",0,0,screenSize.x,screenSize.y)
	else
		drawBg()
		drawClouds()
		Boss.draw()
		Bullets.draw()
		Player.draw()
		Timers.draw()
		Explo.draw()

		drawFg()
		Boss.drawDisplay()
		printLives()

	end

	SpecialMode.draw()


	love.graphics.setCanvas()
	drawGameCanvas()
end

function drawGameCanvas()
	love.graphics.push()
	adaptView()
	love.graphics.draw(screenCanvas)
	love.graphics.pop()
end

function printLives()
	love.graphics.setFont(font)
	love.graphics.setColor(96,248,17)
	local w = printText("Lives ", 2,186)
	love.graphics.setColor(255,158,19 )
	printText(tostring(Player.lives), 2+w, 186)
end

function adaptView()
	local w,h = love.graphics.getWidth(), love.graphics.getHeight()
	local translation
	local ratios = {w/screenSize.x,h/screenSize.y}
	if ratios[1]<ratios[2] then
		ratios[2] = ratios[1]
		translation = {0, -(screenSize.y*ratios[1] - h) * 0.5}
		translation[1] = 0
	else
		ratios[1] = ratios[2]
		translation = {-(screenSize.x*ratios[2] - w) * 0.5, 0}
	end
	local scale = ratios

	love.graphics.translate(translation[1],translation[2])
	love.graphics.scale(scale[1],scale[2])
	-- love.graphics.setScissor(translation[1],translation[2],screenSize.x*scale[1],screenSize.y*scale[2])
	love.graphics.setColor(255,255,255)
end

function love.update(dt)
	-- skip slow frames (below 10fps)
	if dt>1/10 then
		return
	end

	-- dt = dt/4


	updateBg(dt)
	updateClouds(dt)

	if SpecialMode.gameState == "playing" then
		Player.update(dt)
	end

	Explo.update(dt)
	Bullets.update(dt)
	Timers.update(dt)
	Boss.update(dt)
	updateScroll(dt)
	SpecialMode.update(dt)
end

function love.quit()
	love.window.setFullscreen(false)
end

function togglePause() end


function toggleFullscreen()
	love.window.setMode(love.graphics.getWidth(), love.graphics.getHeight(), {
		fullscreen = not love.window.getFullscreen(),
		fullscreentype = "exclusive",
		vsync = default_vsync
	})

	love.mouse.setVisible(not love.window.getFullscreen())
end

function love.keypressed(key)
	if key=="escape" or
		(key=="f4" and (love.keyboard.isDown("ralt") or love.keyboard.isDown("lalt")))
			then
		love.window.setFullscreen(false)
		love.event.push("quit")
		return
	end

	if key == "f11" then
		toggleFullscreen()
	end

	if key == "f3" then
		toggleSound()
	end

	if key=="+" then
		zoomUp()
	end

	if key=="-" then
		zoomDown()
	end

end


function zoomUp()
	local maxZoom = 5
	local az = math.floor(love.graphics.getWidth() / 150 + 0.5)
	local z = math.floor(love.graphics.getWidth() / 300 + 0.5)
	z = z+1
	if z>maxZoom then
		z = maxZoom
	end
	if az==1 then z = 1 end
	love.window.setMode(300*z,200*z)
end

function zoomDown()
	local minZoom = 1
	local z = math.floor(love.graphics.getWidth()/300 + 0.5)
	z = z - 1
	if z<minZoom then
		z = 0.5
	end
	love.window.setMode(300*z, 200*z)
end
